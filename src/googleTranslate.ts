// for debug
// curl -X POST \
// -H "Authorization: Bearer "$(gcloud auth application-default print-access-token) \
// -H "Content-Type: application/json; charset=utf-8" \
// -d '{"sourceLanguageCode": "en","targetLanguageCode": "ja","contents": ["This is a pen."],"mimeType": "text/plain"}' \
// https://translation.googleapis.com/v3/projects/triple-team-284513:translateText
import {projectId location} from "./definitions/apiDefinition";
const text = ['今日から梅雨が明けました。', '初日から非常に暑いです。'];

// Imports the Google Cloud Translation library
const {TranslationServiceClient} = require('@google-cloud/translate').v3beta1;

// Instantiates a client
const translationClient = new TranslationServiceClient();
// node経由で実行するとtsが解釈されてない雰囲気があるので、画面で確認できるようになったらts化していく
const translateText = async (fromLang, toLang, fromText) => {
  // Construct request
  const request = {
    parent: translationClient.locationPath(projectId, location),
    contents: fromText,
    mimeType: 'text/plain', // mime types: text/plain, text/html
    sourceLanguageCode: fromLang,
    targetLanguageCode: toLang,
  };

  try {
    // Run request
    let translatedArray = [];
    const [response] = await translationClient.translateText(request);
    for (const translation of response.translations) {
      translatedArray.push(translation.translatedText);
    }
    return translatedArray;
  } catch (error) {
    console.error(error.details);
  }
};

const enText = translateText("ja", "en", text);
console.log(`Translation: ${enText}`);